class MyAnimation:
    def __init__(self, lines):
        super().__init__()
        frameStarted = False
        self.frames = []
        frame = []
        for line in lines:
            line = line.rstrip()
            if frameStarted:
                if line == "```":
                    frameStarted = False
                    self.frames.append(MyFrame(frame))
                    frame = []
                else:
                    frame.append(line)
            else:
                if line == "```":
                    frameStarted = True

    def MygetFrames(self):
        return self.frames

class MyFrame:
    def __init__(self, frame):
        super().__init__()
        self.frame = frame

    def MygetFrame(self):
        return self.frame

    def Myrender(self):
        for line in self.frame:
            print(line)
