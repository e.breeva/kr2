﻿import time
import os
import keyboard
from Myanimation import MyAnimation, MyFrame

def stopAnim(e):
    global PlayAnim
    PlayAnim = False

filename = "earth.md"

# открытие файла, чтение данных и запуск основной работы
f = open(filename, "r")
lines = f.readlines()
f.close()

# Создаем анимацию из файла
anim = MyAnimation(lines)
# очистка экрана
os.system('cls||clear')
# будем показывать 20 кадров в секунду (по возможности)
frameRate = 20

# остановка анимации по любому нажатию клавиши
keyboard.on_press(stopAnim, True)

PlayAnim = True
while PlayAnim:
    for frame in anim.frames:
        # очистка экрана
        os.system('cls||clear')
        print("Нажмите любую клавишу для выхода\n")
        # отрисовка кадра анимации
        frame.Myrender()
        # Делаем задержку для более плавной отрисовки
        time.sleep(1 / frameRate)
        # проверяем была ли остановлена анимация чтобы выйти из программы
        if (not PlayAnim):
            break
